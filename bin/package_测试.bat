@echo off
echo [INFO] Package the war in target dir.

set MAVEN_OPTS=%MAVEN_OPTS% -XX:MaxPermSize=128m

echo [Step 1] Install all springside modules and archetype to local maven repository.
cd ..\
cd ..\
cd sunflower/modules
call mvn clean install
if errorlevel 1 goto error
cd ..\
goto end
:error
echo Error Happen!!
:end

echo [Step 2] package newcms
cd %~dp0
cd ..
rd /s /q src\main\webapp\upload
call mvn clean package -Dmaven.test.skip=true -Pdev
cd bin
pause