package com.cjf.hairdressing;

import java.util.Map;

import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.handler.SessionHandler;
import com.jfinal.ext.kit.QiniuFileKit;
import com.jfinal.ext.kit.ResourceKit;
import com.jfinal.ext.plugin.quartz.QuartzPlugin;
import com.jfinal.ext.plugin.shiro.CmsShiroInterceptor;
import com.jfinal.ext.plugin.shiro.ShiroPlugin;
import com.jfinal.ext.plugin.sqlinxml.SqlInXmlPlugin;
import com.jfinal.ext.plugin.tablebind.AutoTableBindPlugin;
import com.jfinal.ext.plugin.tablebind.SimpleNameStyles;
import com.jfinal.ext.route.AutoBindRoutes;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.springcat.sunflower.framework.msg.SendMsg;
import com.springcat.sunflower.framework.utils.entity.ConstPlatform;
import com.springcat.sunflower.framework.utils.entity.Controller;
import com.springcat.sunflower.framework.utils.entity.Model;
import com.springcat.sunflower.framework.utils.http.HTTPSender;
import com.springcat.sunflower.framework.utils.shiro.freemarker.ShiroFreeMarkerInterceptor;


/** 
 * @author springCat E-mail:329839705@qq.com 
 * @version 创建时间：2014年4月21日 上午9:57:12 
 */
public class CMSMain extends JFinalConfig {
	
	ActiveRecordPlugin arp;
	ActiveRecordPlugin arp_dustbin;
	Routes routes;
	Map<String,String> m ;
	static String propertyFile= "ProjectConfig.properties";
	public static String dbName;
	
	public void configConstant(Constants me) {

		m = ResourceKit.readProperties(propertyFile);
		
		boolean devMode = getBoolean(m.get("devMode"));
		dbName = devMode ? "sunflower_dev":"sunflower"; 
		me.setDevMode(devMode);


		QiniuFileKit.UPLOAD_RESOURCE_FILE_IMAGE_PATH = m.get("fileAndImagePath"); // 图片、文件资源文件目录
//		QiniuFileKit.DOMAIN_PATH = m.get("ctx"); // 图片、文件资源文件目录
		HTTPSender.httpToAppUrl = m.get("httpToAppUrl");

        if(devMode){
            SendMsg.setMsgUrl("http://tools.hq51.com/msg/send/test");
            QiniuFileKit.RESOURCE_STATIC_IMAG_WEB_PATH = "/test/static/images/web/";
            QiniuFileKit.RESOURCE_IMAG_APP_LAUNCH_PATH = "/test/static/images/appLaunchImages/";
            ConstPlatform.web_redis_port = 6391;
            ConstPlatform.app_redis_port = 6390;
        }else{
            SendMsg.setMsgUrl("http://tools.hq51.com/msg/send");
            QiniuFileKit.RESOURCE_STATIC_IMAG_WEB_PATH = "/static/images/web/";
            QiniuFileKit.RESOURCE_IMAG_APP_LAUNCH_PATH = "/static/images/appLaunchImages/";
            ConstPlatform.web_redis_port = 6381;
            ConstPlatform.app_redis_port = 6380;
        }
	}
	
	public void configRoute(Routes me) {
		this.routes = me;
		me.add(new AutoBindRoutes().addExcludeClasses(Controller.class));
	}

	public void configPlugin(Plugins me) {			
		
		String jdbcUrl = m.get("jdbc.url");
		String dustbinJdbcUrl = m.get("dustbin.jdbc.url");
		String driver = m.get("jdbc.driver");
		String username = m.get("jdbc.username");
		String password = m.get("jdbc.password");
		// Druid
		DruidPlugin druidPlugin = new DruidPlugin(jdbcUrl, username, password, driver);
//		DruidPlugin dustbinDruidPlugin = new DruidPlugin(dustbinJdbcUrl, username, password, driver);
		WallFilter wallFilter = new WallFilter();
		wallFilter.setDbType("mysql");
		druidPlugin.addFilter(wallFilter);
//		dustbinDruidPlugin.addFilter(wallFilter);
		me.add(druidPlugin);
//		me.add(dustbinDruidPlugin);
		
		//add shiro
		me.add(new ShiroPlugin(routes)); 		
		
		//model scan
		//arp = new ActiveRecordPlugin(druidPlugin);
		arp = new AutoTableBindPlugin(druidPlugin,SimpleNameStyles.LOWER_UNDERLINE).addExcludeClasses(Model.class);
//		arp_dustbin = new ActiveRecordPlugin("dustbin", dustbinDruidPlugin); // 垃圾库数据源添加
		arp.setShowSql(true);
		arp.setDevMode(getBoolean(m.get("devMode")));
//		arp_dustbin.setShowSql(true);
//		arp_dustbin.setDevMode(getBoolean(m.get("devMode")));
//		arp.setCache(new EhCache());
		me.add(arp);
//		me.add(arp_dustbin);

		//add quartz
//		if(getBoolean(m.get("quartzMode")))
//			me.add(new QuartzPlugin("quartzjob.properties"));
		
		//添加sqlInXml插件
		me.add(new SqlInXmlPlugin());

	}
	public void configInterceptor(Interceptors me) {
		 me.add(new CmsShiroInterceptor());
		 me.add(new ShiroFreeMarkerInterceptor());
	}

	public void afterJFinalStart() {
		ConstPlatform.setAreaMap();
	}
	
	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler("ctx"));
		me.add(new SessionHandler());
	}
	
	private boolean getBoolean(String s){
		return (new Boolean(s)).booleanValue();
	}
	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		int PORT = 8092;
		propertyFile = "local_ProjectConfig.properties";
		String CONTEXT = "src/main/webapp";
		JFinal.start(CONTEXT, PORT, "/", 5);
	}
}